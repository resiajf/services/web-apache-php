# apache-php

A simple `httpd+php` container based on PHP 7.x, Debian 9 and Apache HTTPD.
`rewrite` and `expires` HTTPD's extensions are enabled. Has the same PHP
extensions as `web-php-fpm` image. Also modifies `httpd` to reduce the number
of processes that creates to reduce the memory usage.